<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-20
 * Time: 12:56
 */

$MESS["NAMES_NAME"] = "Модуль вывода имен";
$MESS["NAMES_DESCRIPTION"] = "Вывод имен";
$MESS["NAMES_PARTNER_NAME"] = "Company";
$MESS["NAMES_PARTNER_URI"] = "http://site.ru/";
$MESS["NAMES_UNINSTALL_TITLE"] = "Деинсталляция модуля";