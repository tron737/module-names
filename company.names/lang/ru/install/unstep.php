<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-20
 * Time: 12:57
 */

$MESS["NAMES_UNINSTALL_TITLE"] = "Деинсталляция модуля";
$MESS["NAMES_UNSTEP_BEFORE"] = "Модуль";
$MESS["NAMES_UNSTEP_AFTER"] = "удален";
$MESS["NAMES_UNSTEP_SUBMIT_BACK"] = "Вернуться в список";