<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-20
 * Time: 12:57
 */

$MESS["NAMES_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
$MESS["NAMES_INSTALL_TITLE"] = "Установка модуля";

$MESS["NAMES_STEP_BEFORE"] = "Модуль";
$MESS["NAMES_STEP_AFTER"] = "установлен";
$MESS["NAMES_STEP_SUBMIT_BACK"] = "Вернуться в список";