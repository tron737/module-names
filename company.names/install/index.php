<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-20
 * Time: 12:58
 */

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;

class company_names extends CModule
{
    public $MODULE_ID = 'company.names';
    public $MODULE_NAME = 'company.names';
    public $pathResources = 'local';
    public $componentPath = '/components/company/names';
    public $apiPath = '/names';

    public $urlRules = [
        "CONDITION" => '#^/names/#',
        "ID" => 'company:names',
        "PATH" => '/names/index.php',
        "RULE" => '',
    ];

    public function __construct()
    {
        if(file_exists(__DIR__."/version.php")) {
            $arModuleVersion = array();

            include_once(__DIR__."/version.php");

            $this->MODULE_ID = str_replace("_", ".", get_class($this));
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME = Loc::getMessage("NAMES_NAME");
            $this->MODULE_DESCRIPTION = Loc::getMessage("NAMES_DESCRIPTION");
            $this->PARTNER_NAME = Loc::getMessage("NAMES_PARTNER_NAME");
            $this->PARTNER_URI = Loc::getMessage("NAMES_PARTNER_URI");
        }

        return false;
    }

    public function DoInstall()
    {
        global $APPLICATION;

        if(CheckVersion(ModuleManager::getVersion("main"), "14.00.00")){

            $this->InstallFiles();
            $this->InstallRouter();
            $this->InstallDB();

            ModuleManager::registerModule($this->MODULE_ID);

            $this->InstallEvents();
        }else{

            $APPLICATION->ThrowException(
                Loc::getMessage("NAMES_INSTALL_ERROR_VERSION")
            );
        }

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("NAMES_INSTALL_TITLE")." \"".Loc::getMessage("NAMES_NAME")."\"", __DIR__."/step.php"
        );

        return false;
    }

    public function DoUninstall()
    {
        global $APPLICATION;

        $this->UnInstallFiles();
        $this->UnInstallRouter();
        $this->UnInstallDB();

        ModuleManager::unRegisterModule($this->MODULE_ID);

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("NAMES_UNINSTALL_TITLE") . " \"" . Loc::getMessage("NAMES_NAME") . "\"",
            __DIR__ . "/unstep.php"
        );

        return false;
    }

    public function UnInstallFiles()
    {
        \Bitrix\Main\IO\Directory::deleteDirectory($_SERVER["DOCUMENT_ROOT"] . $this->apiPath);
        \Bitrix\Main\IO\Directory::deleteDirectory($_SERVER["DOCUMENT_ROOT"] . "/" . $this->pathResources . $this->componentPath);
    }

    public function UnInstallDB()
    {
        Option::delete($this->MODULE_ID);
        global $DB, $APPLICATION;

        if (file_exists($f = dirname(__FILE__).'/db/uninstall.sql'))
        {
            foreach($DB->ParseSQLBatch(file_get_contents($f)) as $sql)
                $DB->Query($sql);
        }
        if ($this->errors !== false)
        {
            $APPLICATION->ThrowException(implode("<br>", $this->errors));
            return false;
        }
        return true;
    }

    public function InstallFiles()
    {
        if(\Bitrix\Main\IO\Directory::isDirectoryExists(__DIR__ . $this->componentPath)) {
            CopyDirFiles(__DIR__ . $this->componentPath, $_SERVER["DOCUMENT_ROOT"] . "/" . $this->pathResources . $this->componentPath, true, true);
        } else {
            throw new \Bitrix\Main\IO\InvalidPathException(__DIR__ . $this->componentPath);
        }

        if(\Bitrix\Main\IO\Directory::isDirectoryExists(__DIR__ . $this->apiPath)) {
            CopyDirFiles(__DIR__ . $this->apiPath, $_SERVER["DOCUMENT_ROOT"] . $this->apiPath, true, true);
        } else {
            throw new \Bitrix\Main\IO\InvalidPathException(__DIR__ . $this->apiPath);
        }
    }

    public function InstallDB()
    {
        global $DB, $APPLICATION;

        if (file_exists($f = dirname(__FILE__).'/db/install.sql'))
        {
            foreach($DB->ParseSQLBatch(file_get_contents($f)) as $sql)
                $DB->Query($sql);

        }
        if ($this->errors !== false)
        {
            $APPLICATION->ThrowException(implode("<br>", $this->errors));
            return false;
        }
        return true;
    }

    public function InstallRouter()
    {
        CUrlRewriter::Add($this->urlRules);
    }

    public function UnInstallRouter()
    {
        $sites = CSite::GetList($by="sort", $order="desc", []);
        $site = $sites->Fetch();
        $this->urlRules['SITE_ID'] = $site['LID'];
        unset($this->urlRules['SORT']);
        unset($this->urlRules['RULE']);
        CUrlRewriter::Delete($this->urlRules);
    }
}