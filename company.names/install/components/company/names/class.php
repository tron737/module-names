<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-20
 * Time: 12:21
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class Names extends CBitrixComponent
{
    /**
     * @var array $defaultUrlTemplates шаблон адреса
     */
    public $defaultUrlTemplates = [
        'index' => 'index.php',
    ];

    /**
     * @var array $variables массив параметр => значение из свойства $defaultUrlTemplates404
     */
    public $variables = [];

    /**
     * @var array $componentVariables массив параметр => значение для компонента
     */
    public $componentVariables = [];

    /**
     * @var array $variableAliases алиасы значений
     */
    public $variableAliases = [];

    /**
     * @var array $templatesUrls сформированный массив шаблон адреса
     */
    public $templatesUrls = [];

    /**
     * @var string $componentPage шаблон отрисовки
     */
    public $componentPage = '';

    /**
     * Иницилизация объектов необходимые для работы
     * @return bool
     */
    protected function InitializeComponent()
    {
        $this->setFrameMode(true);

        $this->templatesUrls = CComponentEngine::makeComponentUrlTemplates($this->defaultUrlTemplates, $this->arParams["SEF_FOLDER"]);

        $this->componentPage = CComponentEngine::parseComponentPath(
            $this->arParams["SEF_FOLDER"],
            $this->templatesUrls,
            $this->variables
        );
        CComponentEngine::initComponentVariables($this->componentPage, $this->componentVariables, $this->variableAliases, $this->variables);

        return true;
    }

    /**
     * Выполнение компонента, главный метод
     * @return mixed|void
     * @throws \Bitrix\Main\SystemException
     */
    public function executeComponent()
    {
        $this->InitializeComponent();

        if (isset($this->arParams) && $this->arParams['USE_CACHE'] == 'Y') {
            $cache_id = md5(serialize($this->arParams));
            $cache_dir = '/' . \company\names\NameTable::getTableName();

            $obCache = new CPHPCache;

            if($obCache->InitCache((isset($this->arParams['CACHE_TIME'])) ? $this->arParams['CACHE_TIME'] : 36000, $cache_id, $cache_dir)) {
                $result = $obCache->GetVars();
            } else {
                $obCache->StartDataCache();

                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($cache_dir);

                $res = \company\names\NameTable::getList([
                    'select' => array("ID","NAME"),
                ]);
                $result = [];
                while ($item = $res->fetch())
                {
                    $CACHE_MANAGER->RegisterTag(\company\names\NameTable::getTableName());
                    $result[] = $item;
                }

                $CACHE_MANAGER->RegisterTag(\company\names\NameTable::getTableName() . "_new");
                $CACHE_MANAGER->EndTagCache();

                $obCache->EndDataCache($result);
            }
        }

//        \company\names\NameTable::add(array(
//            'NAME' => time(),
//        ));

        $this->arResult = array_merge(
            [
                'VARIABLES' => $this->variables,
                'DATA' => $result,
            ],
            $this->arResult
        );

        $this->includeComponentTemplate($this->componentPage);
    }
}