<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-20
 * Time: 13:18
 */

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$resLoad = CModule::IncludeModule("company.names");

$APPLICATION->IncludeComponent(
    'company:names',
    '',
    [
        'SEF_FOLDER' => '/names/',
        'USE_CACHE' => 'Y',
        'CACHE_TIME' => 36000,
    ]
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");