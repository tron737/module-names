<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-20
 * Time: 13:16
 */

namespace company\names;
use Bitrix\Main\Entity;

class NameTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'my_names';
    }

    public static function getUfId()
    {
        return 'MY_NAMES';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\StringField('NAME', [
                'required' => true,
            ]),
            new Entity\DatetimeField('PUBLISH_DATE')
        );
    }
}